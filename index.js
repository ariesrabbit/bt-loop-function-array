// bài một ==================================================================
function showResult() {
  var saveNumber = "";
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 100; j += 10) {
      saveNumber = saveNumber + j + "&nbsp &nbsp &nbsp";
      if (j < 10) {
        saveNumber += "&nbsp&nbsp";
      }
      if (j > 90 && j !== 90) {
        saveNumber += "<br>";
      }
    }
  }
  document.getElementById("txtResult1").innerHTML = `${saveNumber}`;
}

// bài hai===================================================================
var arrNum = [];
var primeNum = [];
// kiểm tra số nguyên tố
function isPrime(n) {
  var flag = 1;
  if (n < 2) return (flag = 2);
  let i = 2;
  while (i < n) {
    if (n % i == 0) {
      flag = 0;
      break;
    }
    i++;
  }
  return flag;
}
// lưu mảng chứa giá trị số nguyên tố
function sortPrimeArr(arr) {
  var newArr = [];
  newArr = arr.filter(function (item) {
    return newArr.includes(item) ? "" : newArr.push(item);
  });
  return newArr;
}
// in kết quả
function printNumber() {
  if (document.querySelector("#inputX").value.trim() == "") {
    return;
  }
  var number = +document.querySelector("#inputX").value;
  arrNum.push(number);
  for (var i = 0; i < arrNum.length; i++) {
    if (isPrime(arrNum[i]) == 1) {
      primeNum.push(arrNum[i]);
    }
  }
  document.querySelector("#txtResult2").innerHTML =
    /*html*/
    `<div>
    <p> Mảng vừa nhập: ${arrNum} </p>
    <p> Số nguyên tố có trong mảng: ${sortPrimeArr(primeNum)} </p>
    </div>`;
}
// bài ba =====================================================================

function calNumber() {
  var sum = 0;
  var number = +document.getElementById("inputN1").value;
  for (var i = 2; i <= number; i++) {
    sum += i;
  }
  sum = sum + 2 * number;
  document.getElementById("txtResult3").innerHTML = `${sum}`;
}
// bài bốn =====================================================================
function findDivisor() {
  var divisor = "";
  var number = +document.getElementById("inputN2").value;
  for (var i = 1; i <= number; i++) {
    if (number % i == 0) {
      divisor += " " + i;
    }
  }
  document.getElementById(
    "txtResult4"
  ).innerHTML = `Ước số của ${number} gồm: ${divisor}`;
}

// bài năm ===============================================
function reverse() {
  const regex = /[0-9]|\d/g; // Regex tìm các kí tự số
  var num = document.getElementById("inputReverseNum").value;
  var newnum1 = num.match(regex); // chuyển các kí tự vừa nhập thành mảng
  newnum1.reverse(); // đảo mảng
  var newnum2 = +newnum1.join(""); // chuyển đổi mảng thành số
  // xóa bỏ các số 0 đứng đầu parseInt(number, 10)
  document.getElementById("txtResult5").innerHTML = ` Số đảo ngược: ${parseInt(
    newnum2,
    10
  )} `;
}
// bài sáu=============================================
function findMin() {
  var max = 0;
  for (index = 1, sum = 0; index < 100; index++) {
    sum += index;
    if (sum >= 100) {
      break;
    }
    max += 1;
  }
  document.getElementById(
    "txtResult6"
  ).innerHTML = ` X lớn nhất thỏa điều kiện: ${max} `;
}
//  bài bảy==============================================
function printMultiplication() {
  var number = +document.getElementById("number-for-multi").value;
  var cal = 0;
  var calSave = "";
  for (var i = 0; i <= 10; i++) {
    cal = number * i;
    calSave += `${number} x ${i} = ${cal} <br>`;
  }
  document.getElementById("txtResult7").innerHTML = calSave;
}
//  bài tám ====================================================
function shareCards() {
  var player = "";
  let cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  for (var i = 0; i < 4; i++) {
    var p = i + 1;
    player +=
      "Player" + p + ": " + cards[i] + cards[i + 4] + cards[i + 8] + "<br>";
  }
  document.getElementById("txtResult8").innerHTML = player;
}

// bài chín==================================================
function find() {
  var chicken = 0;
  for (var i = 1; i < 36; i++) {
    if (i * 2 + (36 - i) * 4 == 100) {
      chicken = i;
    }
  }
  var dog = 36 - chicken;
  document.getElementById(
    "txtResult9"
  ).innerHTML = `Số gà: ${chicken}<br>Số chó ${dog} `;
}
// bài mười==================================================
function findAngle() {
  var minutes = +document.getElementById("inputM").value;
  var hours = +document.getElementById("inputH").value;
  if (hours > 12) {
    hours = hours - 12;
  }
  // 60 phút kim phút quay được 360 độ => 1 phút quay được 6 độ
  // 12 giờ kim giờ quay được 360 độ
  // => 1 giờ kim giờ quay được 360/12 = 30 độ
  // => 1 phút kim giờ quay được 30/60 = 0.5 độ
  var angle = 6 * minutes - 0.5 * (hours * 60);
  document.getElementById(
    "txtResult10"
  ).innerHTML = `Góc giữa kìm giờ và kim phút lúc ${hours} giờ ${minutes} phút là ${Math.abs(
    angle
  )} độ `;
}
